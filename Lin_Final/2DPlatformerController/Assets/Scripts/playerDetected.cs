﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerDetected : MonoBehaviour
{

    public float speed;
    public float knackback;
    public int enemyHealth;
    public Text winText;
    public GameObject player;
    public GameObject bullet;
    public GameObject enemyDestroy;
    public AudioClip HitClip;
    public AudioSource MusicSource;
    public PlayerPlatformerController PPC;

    private Rigidbody2D r2d;
    private Animator animator;
    private SpriteRenderer spriteRenderer;
    bool playDetected;
    float playDistance;


    // Start is called before the first frame update
    void Start()
    {
        enemyHealth = 3;
        speed = 3f;
        Rigidbody2D brb = GetComponent<Rigidbody2D>();
        knackback = 0;
    }

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        r2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float buffer = 0.5f;
        Rigidbody2D brb = GetComponent<Rigidbody2D>();
        if (Vector2.Distance(transform.position, player.transform.position) < 3f)
        {
            playDetected = true;
            animator.SetBool("Walk", true);
            bool flipSprite = (spriteRenderer.flipX ? (transform.position.x + buffer < player.transform.position.x) : (transform.position.x - buffer > player.transform.position.x));
        }


        if (playDetected)
        {
            if (transform.position.x - buffer > player.transform.position.x)
            {
                spriteRenderer.flipX = false;
                brb.velocity = new Vector2(-speed + knackback, brb.velocity.y);
            }
            if (transform.position.x + buffer < player.transform.position.x)
            {
                spriteRenderer.flipX = true;
                brb.velocity = new Vector2(speed + knackback, brb.velocity.y);
            }
            knackback = Mathf.MoveTowards(knackback, 0, Time.deltaTime * 100);
        }

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            MusicSource.clip = HitClip;
            MusicSource.Play();
            speed = -1f;
            Invoke("SpeedBack", 0.5f);
            enemyHealth -= 1;
            if (enemyHealth <= 0)
            {
                PPC.killCount += 1;
                Instantiate(enemyDestroy, gameObject.transform.position, gameObject.transform.rotation);
                gameObject.SetActive(false);
                if (PPC.killCount == 6)
                {
                    winText.text = "There is enough energy to escape from the Gate";
                    Invoke("SetWinTextToNull", 2f);
                }
            }
        }
    }

    void SpeedBack()
    {
        speed = 3f;
    }

    void SetWinTextToNull()
    {
        winText.text = "";
    }

}
