﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerPlatformerController : PhysicObject
{
    // speed variable for player and jump action
    public float maxSpeed = 5;
    public float jumpTakeOffSpeed = 5;
    public int killCount;
    // Text for "Count", "Win", "Weapon Access", and "Count Down"
    public Text countText;
    public Text WinText;
    public Text Weapontext;
    public Text fireText;
    // Import the weapon script to have access for the bool to check if the player has the weapon
    public weapon weaponScript;
    public playerDetected playerDetected;
    public GameObject Enemy;
    public AudioClip hurtClip;
    public AudioSource MusicSource;


    // initiate spriteRenderer and annimator of player
    private SpriteRenderer spriteRenderer;
    private Animator animator;


    // variables that works with text and count down system of the game
    private int health;
    private bool gameFinished;
    private bool doorOpen;
    private int min;
    private int sec;
    private string Sec;

    float knackback;


    private void Start()
    {
        // at the beginning of the game, set the time count down and other instance for the game
        weaponScript.Fire = 5;
        gameFinished = false;
        health = 5;
        SetCountText();
    }


    // The function to update the gems that player collect

    // Use this for initialization
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }


    protected override void ComputeVelocity()
    {
        // player's controller for moving and jumping
        Vector2 move = Vector2.zero;

        move.x = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump") && grounded)
        {
            velocity.y = jumpTakeOffSpeed;
        }
        else if (Input.GetButtonUp("Jump"))
        {
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * 0.5f;
            }
        }


        // bool to check if the current sprite of the player to see if we need to flip the player's sprite by x axis
        bool flipSprite = (spriteRenderer.flipX ? (move.x > 0.01f) : (move.x < 0.01f));
        if (flipSprite)
        {
            spriteRenderer.flipX = !spriteRenderer.flipX;
            // when the sprite is fliped, the weapon need to flip as well
            weaponScript.hasFlip = !weaponScript.hasFlip;
            // rotate the weapon 
            weaponScript.firepoint.transform.Rotate(0f, 180f, 0f);
        }

        animator.SetBool("grounded", grounded);
        animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);

        targetVelocity = move * maxSpeed + new Vector2(knackback, 0);
        knackback = Mathf.MoveTowards(knackback, 0, Time.deltaTime * 5);

        SetFireText();

    }

    // All the collisions that player has
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // when the player run into the Gate
        if (collision.gameObject.CompareTag("Win Gate"))
        {
            if (killCount >= 6)
            {
                WinText.text = "You have survived";
                gameFinished = true;
                Invoke("RestartLevel", 1.3f);
            }
            else
            {
                WinText.text = "The gate needs energy from the creatures in the map";
                Invoke("SetWinTextToNull", 2f);
            }
        }

        // when the player run into the Gem, collect it
        if (collision.gameObject.CompareTag("Gem"))
        {
            health++;
            SetCountText();
        }

        // when the player run into the fire weapon
        if (collision.gameObject.CompareTag("Fire"))
        {
            // set the weapon bool to true to give the player the access to shoot
            weaponScript.Fire += 5;
            SetFireText();
            collision.gameObject.SetActive(false);
            Weapontext.text = "You have collected 5 fire bullets, press Z to shoot";
            Invoke("SetWeaponTextToNull", 2f);
        }


        if (collision.gameObject.CompareTag("Enemy"))
        {
            MusicSource.clip = hurtClip;
            MusicSource.Play();
            animator.SetBool("hurt", true);
            if (health == 1)
            {
                WinText.text = "You are dead";
                gameFinished = true;
                Invoke("RestartLevel", 1.3f);
            }
            Invoke("SetHurtToFalse", 0.4f);
            health--;
            SetCountText();
            if (Enemy.transform.position.x > transform.position.x)
            {
                knackback = -5;
            }

            if (Enemy.transform.position.x < transform.position.x)
            {
                knackback = 5;
            }
        }



    }

    void SetCountText()
    {
        countText.text = "Health: " + health.ToString();
    }

    void SetFireText()
    {
        fireText.text = "Fire bullet: " + weaponScript.Fire.ToString();
    }

    // function to restart the game
    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    // function to set the win text to null, the purpose is to show text only for 2 seconds
    void SetWinTextToNull()
    {
        WinText.text = "";
    }

    // function to set the weapon text to null
    void SetWeaponTextToNull()
    {
        Weapontext.text = "";
    }

    void SetHurtToFalse()
    {
        animator.SetBool("hurt", false);
    }

}
