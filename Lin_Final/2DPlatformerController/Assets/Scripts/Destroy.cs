﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{
    public AudioClip DestroyClip;
    public AudioSource MusicSource;
    // Start is called before the first frame update
    void Start()
    {
        MusicSource.clip = DestroyClip;
        MusicSource.Play();
        Invoke("DestroyEffect", 0.8f);
    }

    void DestroyEffect()
    {
        gameObject.SetActive(false);
    }


}
