﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playerSpawn : MonoBehaviour
{
    public GameObject player;

    private float spawnSpot;


    // Start is called before the first frame update
    void Start()
    {
        spawnSpot = Random.Range(0, 3);
        print(spawnSpot);
        switch (spawnSpot)
        {
            case 0:
                player.transform.position = new Vector3(-33, 33, 0);
                break;
            case 1:
                player.transform.position = new Vector3(-46, 35, 0);
                break;
            case 2:
                player.transform.position = new Vector3(-14, 36, 0);
                break;
            default:
                break;
        }
    }




    void BackToMenus()
    {
        SceneManager.LoadScene("_MenusScene");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("escape")){
            Application.Quit();
            Debug.Log("Quit");
        }    
    }
}
